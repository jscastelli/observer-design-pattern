#ifndef SUBSCRIBERONE_HPP
#define SUBSCRIBERONE_HPP

#include "SubscriberInterface.hpp"
#include "DataPublisher.hpp"

class SubscriberOne : public SubscriberInterface
{
   public:
      SubscriberOne(DataPublisher* aDataPublisher = nullptr);
      void Update(PublisherInterface* aPublisher);
   private:
      PublisherInterface* mPublisher{ nullptr };
};
#endif