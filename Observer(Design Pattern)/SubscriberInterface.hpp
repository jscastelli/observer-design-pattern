#ifndef SUBSCRIBERINTERFACE_HPP
#define SUBSCRIBERINTERFACE_HPP

#include "PublisherInterface.hpp"

class SubscriberInterface
{
public:
   virtual void Update(PublisherInterface* aPublisher) = 0;
};
#endif