#include "SubscriberOne.hpp"
#include <iostream>

SubscriberOne::SubscriberOne(DataPublisher* aDataPublisher)
{
   if (aDataPublisher)
   {
      aDataPublisher->AddSubscriber(this);
      mPublisher = aDataPublisher;
   }
}

void SubscriberOne::Update(PublisherInterface* aPublisher)
{
   DataPublisher* aDataPublisher = dynamic_cast<DataPublisher*>(aPublisher);
   if (aDataPublisher)
   {
      std::cout << "Subscriber 1 Updated" << std::endl;
      std::cout << "  Temperature: " << aDataPublisher->GetTemperature() << std::endl;
      std::cout << "     Humidity: " << aDataPublisher->GetHumidity() << std::endl;
   }
}
