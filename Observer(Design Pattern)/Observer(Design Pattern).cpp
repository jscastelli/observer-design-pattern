
#include <iostream>
#include "DataPublisher.hpp"
#include "SubscriberOne.hpp"
#include "SubscriberTwo.hpp"

int main()
{
   DataPublisher* dataPublisher = new DataPublisher();

   SubscriberOne* subscriberOne = new SubscriberOne(dataPublisher);
   SubscriberTwo* subscriberTwo = new SubscriberTwo(dataPublisher);

   dataPublisher->SetData(10.0, 12.0);
   dataPublisher->SetData(20.0, 0.0);
   dataPublisher->SetData(30.0, 20.0);
   dataPublisher->SetData(10.0, 40.0);
   dataPublisher->SetData(50.0, 30.0);
   dataPublisher->SetData(100.0, 80.0);
}
